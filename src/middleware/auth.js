const jwt = require('jwt-simple');
const datasource = require('../models/datasource');
const { User } = datasource().models;
const { JWT_SECRET } = require('../../config/server');
const secret = JWT_SECRET;

exports.authentication = (req, res, next) => {
  if (!req.headers.token) {
    req.isLoggedIn = false;
    next();
    return;
  }

  let data = null;

  try {

    data = jwt.decode(req.headers.token, secret, true);
  } catch (exception) {
    req.isLoggedIn = false;
    next();
    return;
  }

  User.findOne({
    where: {
      id: data.id,
      email: data.email,
      external_id: data.external_id
    },
  })
    .then(result => {
      // Check if the result really does match the decoded JWT
      if(
        !result ||
        data.id !== result.id ||
        data.email !== result.email ||
        data.external_id !== result.external_id
      ){
        throw new Error('User in DB does not match user in JWT. Aborting login op.');
      }

      // add user info to req.user for later use
      const { id, username, email, avatar_url, usergroup } = result.dataValues;
      req.user = { id, username, email, avatar_url, usergroup };

      req.isLoggedIn = true;
      next();
    })
    .catch(err => {
      req.isLoggedIn = false;
      next();
    });
};
