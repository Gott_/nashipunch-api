const redis = require("redis");

const { REDIS_PORT, REDIS_HOST } = require("../../config/server");

const PORT = REDIS_PORT;
const HOST = REDIS_HOST;

const client = redis.createClient({
  host: HOST,
  port: PORT
});

client.on("connect", () => {
  console.log("=========== Redis is connected ===========");
});

client.on("error", err => {
  console.log(`Redis Error: ${err}`);
});

module.exports = client;