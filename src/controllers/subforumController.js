const httpStatus = require('http-status');
const datasource = require('../models/datasource');

const composer = require('../helpers/queryComposer')
const Response = require('../helpers/responseStrategy')
const Pagination = require('../helpers/PaginationBuilder')

const redis = require('../services/redisClient')

const { Subforum } = datasource().models;

exports.index = async (req, res) => {
  const scope = composer.scope(req, Subforum);
  const options = composer.options(req, Subforum.blockedFields);

  options.distinct = true;
  options.col = 'Subforum.id';

  try {    
    result = await scope.findAndCountAll(options);

    const pagResult = new Pagination(options, req, result).build();
    redis.setex(composer.keyCache(req), 1, JSON.stringify(pagResult));

    Response.send(res, pagResult, options);
  } catch (error) {
    console.error(error);
    Response.send(res, error, options);
  }
};
exports.show = async (req, res) => {
  const scope = composer.scope(req, Subforum);
  const options = composer.options(req, Subforum.blockedFields);

  options.distinct = true;
  options.col = 'Subforum.id';

  try {    
    result = await scope.findOne(options);

    redis.setex(composer.keyCache(req), 1, JSON.stringify(result));

    Response.send(res, result, options);
  } catch (error) {
    console.error(error);
    Response.send(res, error, options);
  }
};
exports.store = async (req, res) => {
  const scope = composer.scope(req, Subforum);  

  try {
    const subforum = await Subforum.create(req.body);

    const result = await scope.findOne({ where: { id: subforum.id } });

    res.status(httpStatus.CREATED);
    res.json(result);
  } catch (exception) {
    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    res.json(exception);
  }
};
exports.update = async (req, res) => {
  const scope = composer.scope(req, Subforum);

  try {
    const subforum = await Subforum.findOne({ where: req.params });

    await subforum.update(req.body);

    const result = await scope.findOne({ where: req.params });

    res.status(httpStatus.OK);
    res.json(result);
  } catch (err) {
    res.status(httpStatus.BAD_REQUEST);
    res.status(httpStatus.UNPROCESSABLE_ENTITY).send(err.message);
  }
};
exports.destroy = async (req, res) => {   
  Subforum.destroy({ where: req.params })
    .then(() => res.sendStatus(httpStatus.NO_CONTENT))
    .catch(() => res.status(httpStatus.UNPROCESSABLE_ENTITY));
};
exports.count = (req, res) => {
  const options = composer.onlyQuery(req);

  Subforum.count(options)
    .then((result) => {
      res.status(httpStatus.OK);
      res.json(result);
    })
    .catch((err) => {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      res.json(err.message);
    });
};