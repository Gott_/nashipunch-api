const httpStatus = require('http-status');

class Response {
  constructor(result) {
    this.result = result;
  }

  sendAction(res, options) {
    throw Error('Response strategy not defined.');
  }
}

class ErrorResponse extends Response {
  sendAction(res) {
    switch (this.result.message) {
      case 'Validation Error':
        res.status(httpStatus.UNPROCESSABLE_ENTITY);
        res.json({ message: 'Unprocessable entity.' });
        break;
      default:
        res.status(httpStatus.SERVICE_UNAVAILABLE);
        res.json({ message: 'Service unavailable.' });
    }
  }
}

class NotFoundResponse extends Response {
  sendAction(res) {
    res.status(httpStatus.NOT_FOUND);
    return res.json({
      message: 'Resource not found.',
    });
  }
}

class ResponseOK extends Response {
  sendAction(res) {
    res.status(httpStatus.OK);
    return res.json(this.result);
  }
}

class PaginatedResponse extends Response {
  sendAction(res, options) {
    if (options.page === 1) {
      res.status(httpStatus.OK);
      return res.json(this.result);
    }
    return new NotFoundResponse(this.result).sendAction(res, options);
  }
}

const responseStrategy = () => {
  const selectStrategy = (result) => {
    switch (true) {
      case result === null: {
        return new NotFoundResponse(result);
      }

      case typeof result === 'object' && 'list' in result: {
        return new ResponseOK(result);
      }

      case result instanceof Error: {
        return new ErrorResponse(result);
      }

      case 'list' in result: {
        return new PaginatedResponse(result);
      }

      default: {
        return new ResponseOK(result);
      }
    }
  };

  return {
    send(response, result, options) {
      selectStrategy(result).sendAction(response, options);
    },
  };
};

module.exports = responseStrategy();