'use strict';
module.exports = (sequelize, DataTypes) => {
  const Thread = sequelize.define('Thread', {
    title: DataTypes.STRING,
    icon_id: DataTypes.INTEGER,
    user_id: DataTypes.INTEGER,
    subforum_id: DataTypes.INTEGER,
    createdAt: {type: DataTypes.DATE, field: 'created_at'},
    updatedAt: {type: DataTypes.DATE, field: 'updated_at'},
  }, {
    timestamps: true,    
  });
  Thread.associate = function(models) {
    // A user can have many posts
    // Thread.Posts = Thread.hasMany(models.Post, { as: 'posts', foreignKey: 'thread_id' });
    Thread.User = Thread.belongsTo(models.User, { as: 'user', foreignKey: 'user_id' });
    Thread.Subforum = Thread.belongsTo(models.Subforum, { as: 'subforum', foreignKey: 'subforum_id' });
  };
  Thread.initScopes = () => {
    Thread.addScope('withPosts', () => ({
      include: [
        {
          association: Thread.Posts,
          attributes: ['id', 'content', 'user_id', 'created_at', 'updated_at'],
        },
        {
          association: Thread.User,
          attributes: ['username', 'usergroup', 'created_at', 'avatar_url'],
        },
      ],
    }));
  }
  return Thread;
};
