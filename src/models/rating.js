'use strict';
module.exports = (sequelize, DataTypes) => {
  const Rating = sequelize.define('Rating', {
    rating_type: DataTypes.INTEGER,
    post_id: DataTypes.INTEGER.UNSIGNED,
    createdAt: {type: DataTypes.DATE, field: 'created_at'},
    updatedAt: {type: DataTypes.DATE, field: 'updated_at'},
  }, {
    timestamps: true
  });
  Rating.associate = function(models) {
    Rating.belongsTo(models.Post);
  };
  Rating.initScopes = () => {}
  return Rating;
};