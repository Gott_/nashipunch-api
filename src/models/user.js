'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    username: DataTypes.STRING,
    email: DataTypes.STRING,
    avatar_url: DataTypes.STRING,
    usergroup: DataTypes.INTEGER,
    external_type: DataTypes.STRING,
    external_id: DataTypes.STRING,
    createdAt: {type: DataTypes.DATE, field: 'created_at'},
    updatedAt: {type: DataTypes.DATE, field: 'updated_at'},
  }, {
    timestamps: true
  });
  User.associate = function(models) {
    // A user can have many posts
    User.hasMany(models.Post, {foreignKey: 'user_id'});
    User.hasMany(models.Thread, {foreignKey: 'user_id'});
};
User.initScopes = () => {}
  return User;
};