// config/config.js
'use strict';
require('dotenv').config();

module.exports = {
    "production": {
        "username": process.env.DB_USERNAME,
        "password": process.env.DB_PASSWORD,
        "database": process.env.DB_NAME,
        "host": process.env.DB_HOST,
        "dialect": "mysql",
        GOOGLE_CLIENT_ID: '369362779903-tisfmnpfi495l802bgoliu12p67vcbbq.apps.googleusercontent.com',
        JWT_SECRET: '123654654'
    },
    "development": {
        "username": process.env.DB_USERNAME_DEV,
        "password": process.env.DB_PASSWORD_DEV,
        "database": process.env.DB_NAME_DEV,
        "host": process.env.DB_HOST_DEV,
        "dialect": "mysql",
        GOOGLE_CLIENT_ID: '369362779903-tisfmnpfi495l802bgoliu12p67vcbbq.apps.googleusercontent.com',
        JWT_SECRET: '123654654'
    }
};